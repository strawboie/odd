﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class PlayerInput : NetworkBehaviour {
    PlayerManager player;
    private Image ui;

    public float jumpVelocity = 0.5f;
    [SyncVar]
    bool feetActive = false;

    [SyncVar]
    float jumpEnergy = 0.0f;

    private AudioSource jumpAudio;
    public GameObject feet;
    Rigidbody2D rigidbody2d;
	// Use this for initialization
	void Start () {
        player = GetComponent<PlayerManager>();
        rigidbody2d = GetComponent<Rigidbody2D>();
        jumpAudio = GetComponent<AudioSource>();
    }

	public override void OnStartLocalPlayer()
	{
        base.OnStartLocalPlayer();
        GameObject tempObject = GameObject.Find("EnduranceBar");
        ui = tempObject.GetComponent<Image>();
        ui.fillAmount = jumpEnergy / 100.0f;
	}

    void Update()
    {
        if (isServer)
        {
            jumpEnergy = jumpEnergy + 50.0f * Time.deltaTime;

            if (jumpEnergy > 100)
            {
                jumpEnergy = 100;
            }
        }

        if (isLocalPlayer)
        {
            ui.fillAmount = jumpEnergy / 100.0f;

            if (Input.GetKeyDown(KeyCode.Space) && jumpEnergy >= 99)
            {
                Debug.Log("Jump");
                rigidbody2d.velocity += jumpVelocity * Vector2.up;
                jumpAudio.Play();
                CmdJump();
            }
        }
    }

	// Update is called once per frame
	void FixedUpdate ()
    {
        if (isServer)
        {
            if (rigidbody2d.velocity.y < 0)
            {
                feetActive = true;
            }
            else
            {
                feetActive = false;
            }
        }

        feet.SetActive(feetActive);

        if (!isLocalPlayer)
            return;

        player.move(Input.GetAxisRaw("Horizontal"));
    }

    [Command]
    public void CmdJump()
    {
        jumpEnergy = 0;
    }

    [Command]
    public void CmdKill()
    {
        player.isAlive = false;
    }
}
