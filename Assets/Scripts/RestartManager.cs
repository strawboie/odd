﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class RestartManager : MonoBehaviour 
{
    public GameObject restart;
    public GameObject restartWait;

    PlayerManager player;

    int maxPlayers = int.MaxValue;

	public void showRestartMenu(int maxPlayers, PlayerManager player)
    {
        this.player = player;
        this.maxPlayers = maxPlayers;
        restart.SetActive(true);
    }

    public void showRestartingPlayers()
    {
        restart.SetActive(false);
        restartWait.SetActive(true);
        restartWait.GetComponent<UI_RestartWait>().Initialize(maxPlayers, player);
    }
}
