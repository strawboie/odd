﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class PlayerManager : NetworkBehaviour {

    public float maxSpeed = 20.0f;
    public float jumpVelocity = 0.5f;
    [SyncVar, HideInInspector]
    public bool isAlive = true;
    bool isDestroying = false;

    float electricityTime = 0.0f;

    public Sprite[] sprites;

    [SyncVar]
    int spriteNum;
    [HideInInspector]
    public Animator deathAnimator;
    //protected bool isGrounded = true;

    protected Rigidbody2D body;
    private Animator animator;
    public AudioClip deathSound;
    bool hasStartedRestartMenu = false;

    [SyncVar, HideInInspector]
    public int playersRestarting = 0;
    [SyncVar, HideInInspector]
    public bool gameOver = false;

    [SyncVar]
    int maxPlayers = 0;

    public void addPlayerReadyForRestart()
    {
        if (gameOver && isLocalPlayer)
        {
            CmdAddPlayerForRestart();
        }
    }

    [Command]
    void CmdAddPlayerForRestart()
    {
        foreach (PlayerInput playerInput in GameObject.FindObjectsOfType<PlayerInput>())
        {
            playerInput.gameObject.GetComponent<PlayerManager>().playersRestarting++;
        }
    }

	public override void OnStartServer()
	{
        spriteNum = Random.Range(0, 8);

        if (Prototype.NetworkLobby.LobbyManager.s_Singleton != null)
            maxPlayers = Prototype.NetworkLobby.LobbyManager.s_Singleton._playerNumber;
	}

	public override void OnStartLocalPlayer()
	{
		base.OnStartLocalPlayer();

        SpriteRenderer[] children = GetComponentsInChildren<SpriteRenderer>();

        foreach (SpriteRenderer child in children)
        {
            if (child.transform.parent == gameObject.transform)
            {
                child.color = new Color(child.color.r, child.color.g, child.color.b, 1.0f);
                break;
            }
        }
	}

	// Use this for initialization
	void Start()
    {
        body = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        if (playersRestarting == maxPlayers && maxPlayers > 0 && isServer)
        {
            Prototype.NetworkLobby.LobbyManager.s_Singleton.ServerChangeScene(Prototype.NetworkLobby.LobbyManager.s_Singleton.playScene);
        }

        if (!gameOver)
        {
            GetComponent<SpriteRenderer>().sprite = sprites[spriteNum];

            if (!isAlive && !isDestroying)
            {
                isDestroying = true;
                GameObject tempObject = GameObject.Find("AnimationManager");
                deathAnimator = tempObject.GetComponent<Animator>();

                if (isServer)
                {
                    foreach (Collider2D collider in GetComponentsInChildren<Collider2D>())
                    {
                        collider.enabled = false;
                    }

                    RpcshowBunnyKillOnClient();
                    if (GetComponent<PlayerInput>() == null)
                        StartCoroutine(waitAnimationFinish(deathAnimator));
                    else
                    {
                        foreach (PlayerInput playerInput in GameObject.FindObjectsOfType<PlayerInput>())
                        {
                            playerInput.gameObject.GetComponent<PlayerManager>().gameOver = true;
                        }
                    }
                }
            }

            if (isServer)
            {
                electricityTime += Time.deltaTime;
                if (electricityTime > 2.5f)
                    electricityTime = 0.0f;
            }
        }
        else if (gameOver && isLocalPlayer && !hasStartedRestartMenu)
        {
            RestartManager restartManager = GameObject.Find("PlayerUI").GetComponent<RestartManager>();
            restartManager.showRestartMenu(maxPlayers, this);
            hasStartedRestartMenu = true;
        }
    }

    [ClientRpc]
    void RpcshowBunnyKillOnClient()
    {
        isDestroying = true;
        GameObject tempObject = GameObject.Find("AnimationManager");
        foreach (Collider2D collider in GetComponentsInChildren<Collider2D>())
        {
            collider.enabled = false;
        }
        GetComponent<SpriteRenderer>().color = Color.clear;
        Transform animationPos = tempObject.GetComponent<Transform>();
        animationPos.SetPositionAndRotation(transform.position, transform.rotation);

        deathAnimator = tempObject.GetComponent<Animator>();
        deathAnimator.SetTrigger("use");
        //deathAnimator.Play();
        AudioSource deathAudio = tempObject.GetComponent<AudioSource>();
        deathAudio.Play();

        if (GetComponent<PlayerInput>() != null)
        {
            if (isLocalPlayer)
            {
                Text youLose = GameObject.Find("You Lose").GetComponent<Text>();
                youLose.color = new Color(youLose.color.r, youLose.color.g, youLose.color.b, 1.0f);
            }
            else
            {
                Text youLose = GameObject.Find("You Win").GetComponent<Text>();
                youLose.color = new Color(youLose.color.r, youLose.color.g, youLose.color.b, 1.0f);
            }

            foreach (PlayerInput playerInput in GameObject.FindObjectsOfType<PlayerInput>())
            {
                playerInput.gameObject.GetComponent<PlayerManager>().gameOver = true;
            }
        }
    }

    IEnumerator waitAnimationFinish(Animator anim)
    {
        yield return new WaitForSeconds(anim.GetCurrentAnimatorStateInfo(0).length + anim.GetCurrentAnimatorStateInfo(0).normalizedTime);
        NetworkServer.Destroy(gameObject);
        yield return null;
    }

    public void move(float horizontalInput)
    {
        Vector2 movementVelocity = body.velocity;        
        movementVelocity.x = maxSpeed * horizontalInput;
        body.velocity = movementVelocity;
    }

	public void OnTriggerStay2D(Collider2D collision)
	{
        if (!isDestroying && isServer)
        {
            if (LayerMask.LayerToName(collision.gameObject.layer) == "Electricity")
            {
                if (electricityTime > 2.0f)
                    isAlive = false;
            }
        }
	}
}
