﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class BunnyHead : MonoBehaviour 
{
    public PlayerManager playerManager;

	void OnCollisionEnter2D(Collision2D collision)
	{
        if (playerManager.isServer && LayerMask.LayerToName(collision.collider.gameObject.layer) == "BunnyFeet")
        {
            Debug.Log("Kill");
            playerManager.isAlive = false;
        }
	}
}
