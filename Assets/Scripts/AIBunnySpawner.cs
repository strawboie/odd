﻿using UnityEngine;
using UnityEngine.Networking;

public class AIBunnySpawner : NetworkBehaviour
{

    public GameObject enemyPrefab;
    public int numberOfEnemies;

    public override void OnStartServer()
    {
        for (int i = 0; i < numberOfEnemies; i++)
        {
            var spawnPosition = new Vector3(
                Random.Range(-2.0f, 2.0f) + transform.position.x,
                Random.Range(-1.0f, 0.5f) + transform.position.y,
                0.0f);

            var enemy = (GameObject)Instantiate(enemyPrefab, spawnPosition, Quaternion.identity);
            enemy.GetComponent<AIInput>().setSeed(Random.Range(10, 1000));
            NetworkServer.Spawn(enemy);
        }
    }
}