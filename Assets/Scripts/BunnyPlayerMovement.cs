﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class BunnyPlayerMovement : NetworkBehaviour 
{
    public float movementSpeed = 5.0f;
    Rigidbody2D rigidbody2d;
    bool isGrounded = false;

	// Use this for initialization
	void Start () 
    {
        rigidbody2d = GetComponent<Rigidbody2D>();	
	}

	// Update is called once per frame
	private void FixedUpdate()
	{
        if (!isLocalPlayer)
            return;

        float movementX = 0;
        if (Input.GetKey(KeyCode.RightArrow))
            movementX += movementSpeed;

        if (Input.GetKey(KeyCode.LeftArrow))
            movementX -= movementSpeed;

        if (Input.GetKeyDown(KeyCode.Space) & isGrounded)
        {
            Debug.Log("Add force");
            rigidbody2d.AddForce(new Vector2(0, 5), ForceMode2D.Impulse);
        }

        rigidbody2d.velocity = new Vector2(movementX * Time.deltaTime, rigidbody2d.velocity.y);
	}

	private void OnCollisionEnter2D(Collision2D collision)
	{
        if (collision.gameObject.layer == LayerMask.NameToLayer("Platform"))
        {
            Debug.Log("grounded");
            isGrounded = true;
        }
	}

	private void OnCollisionExit2D(Collision2D collision)
	{
        if (collision.gameObject.layer == LayerMask.NameToLayer("Platform"))
        {
            Debug.Log("airborne");
            isGrounded = false;
        }
	}
}
