﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ClientDiscovery : NetworkDiscovery {

    public Prototype.NetworkLobby.LobbyServerList lobbyServerList;

    public override void OnReceivedBroadcast(string fromAddress, string data)
    {
        lobbyServerList.addIPToServerList(fromAddress);
        Debug.Log("Received broadcast from: " + fromAddress + " with the data: " + data);
    }
}
