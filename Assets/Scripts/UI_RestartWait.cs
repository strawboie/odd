﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class UI_RestartWait : MonoBehaviour 
{
    public GameObject missingSnitchPrefab;
    public Transform verticalLayoutParent;

    List<GameObject> snitches;
    PlayerManager player;

    Prototype.NetworkLobby.LobbyManager lobby;
	// Use this for initialization
	void Awake () 
    {
        snitches = new List<GameObject>();
	}

	private void Update()
	{
        Debug.Log("Restarting players: " + player.playersRestarting);
        for (int i = 0; i < player.playersRestarting; i++)
        {
            snitches[i].GetComponent<Image>().color = Color.white;
        }
	}

	public void Initialize(int snitchCount, PlayerManager player)
    {
        this.player = player;
        for (int i = 0; i < snitchCount; i++)
        {
            GameObject snitch = (GameObject)Instantiate(missingSnitchPrefab, verticalLayoutParent);
            snitches.Add(snitch);
        }

        player.addPlayerReadyForRestart();
    }
}
