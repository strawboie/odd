﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class AIInput : NetworkBehaviour 
{
    public float turnChance = 0.001f;

    PlayerManager bunny;
    bool movingRight = true;
    Rigidbody2D body;
    // Use this for initialization
    void Start()
    {
        body = GetComponent<Rigidbody2D>();
        bunny = GetComponent<PlayerManager>();
    }

    public void setSeed(int seed)
    {
        Random.InitState(seed);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (!isServer)
            return;

        if (Random.Range(0.0f, 1.0f) < turnChance)
        {
            movingRight = !movingRight;
        }

        if (movingRight)
        {
            bunny.move(1.0f);
        }
        else if (!movingRight)
        {
            bunny.move(-1.0f);
        }
    }
}
