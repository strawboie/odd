﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;
using UnityEngine.Networking.Types;
using System.Collections;

namespace Prototype.NetworkLobby
{
    public class LobbyServerEntry : MonoBehaviour 
    {
        public Text serverInfoText;
        public Button joinButton;

		public void Populate(string ip, LobbyManager lobbyManager, RectTransform lobbyPanel, Color c)
		{
            serverInfoText.text = ip;

            joinButton.onClick.RemoveAllListeners();
            joinButton.onClick.AddListener(() => { JoinMatch(ip, lobbyPanel, lobbyManager); });

            GetComponent<Image>().color = c;
        }

        void JoinMatch(string ip, RectTransform lobbyPanel, LobbyManager lobbyManager)
        {
			/*lobbyManager.matchMaker.JoinMatch(networkID, "", "", "", 0, 0, lobbyManager.OnMatchJoined);
			lobbyManager.backDelegate = lobbyManager.StopClientClbk;
            lobbyManager._isMatchmaking = true;
            lobbyManager.DisplayIsConnecting();*/

            lobbyManager.ChangeTo(lobbyPanel);

            lobbyManager.networkAddress = ip;
            lobbyManager.StartClient();

            lobbyManager.backDelegate = lobbyManager.StopClientClbk;
            lobbyManager.DisplayIsConnecting();

            lobbyManager.SetServerInfo("Connecting...", lobbyManager.networkAddress);
        }
    }
}